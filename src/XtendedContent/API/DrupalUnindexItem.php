<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\xtcsearch\XtendedContent\API\UnindexItem;

class DrupalUnindexItem extends DrupalIndexBase {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return string|void
   */
  public static function processIndex(EntityInterface $entity) {
    $profile = static::getProfile('unindex', $entity->getEntityTypeId(), $entity->bundle());
    if (!empty($profile)) {
      try {
        $options = [
          'id' => $entity->id(),
          'type' => '_doc',
          'index' => static::getIndexName(static::getIndexOptions($entity)),
        ];
        $handler = UnindexItem::delete($profile, $options);

        $msg = $handler->content();
        if (is_string($msg)) {
          $msg = Json::decode($msg);
        }
        $message = $msg['result'] . ' ——— ' . 'Content unindexed: '
          . $entity->getEntityTypeId() . ' — '
          . $entity->id() . ' in index: ' . $options['index'];
        \Drupal::logger('xtcdrupal_search')->debug($message);
      } catch (\Exception $exception) {
        $message = $exception->getMessage() . ' (' . $exception->getCode() . ')';
        \Drupal::logger('xtcdrupal_search')->warning($message);
      } finally {
        return $message;
      }
    }
    $message = "Profile not found — Content could not be unindexed.";
    return $message;
  }

}
