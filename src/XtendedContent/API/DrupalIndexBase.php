<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Site\Settings;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcsearch\XtendedContent\API\XtcSearchLoaderSearch;

abstract class DrupalIndexBase {

  const INDEX_NAME = 'xtcdrupal';

  const PROFILE_NAME = 'drupal_content';

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return string|void
   */
  public static function processIndex(EntityInterface $entity) {
    $message = "";
    return $message;
  }

  /**
   * @param $action
   * @param $entityType
   * @param $bundle
   *
   * @return mixed|null
   */
  protected static function getProfile($action, $entityType, $bundle) {
    if (!empty(static::getProfileName())) {
      $settings = XtcSearchLoaderSearch::load(static::getProfileName());
      if (!empty($settings[$action])) {
        if (!empty($settings[$action][$entityType])) {
          return $settings[$action][$entityType][$bundle] ?? NULL;
        }
      }
    }
    return NULL;
  }

  protected static function getProfileName() {
    $settings = Settings::get('xtcdrupal');
    if (!empty($settings)) {
      return $settings['profile'] ?? self::PROFILE_NAME;
    }
    return self::PROFILE_NAME;
  }

  /**
   * @param array $options
   *
   * @return mixed|string
   */
  protected static function getIndexName($options = []) {
    if (!empty($options['index'])) {
      return $options['index'];
    }
    if (!empty($options['profile'])) {
      $profile = XtcLoaderProfile::load($options['profile']);
      return $profile['index'] ?? static::INDEX_NAME;
    }
    return static::INDEX_NAME;
  }

  /**
   * @param $profile
   *
   * @return mixed|string
   */
  protected static function getMapping($profile) {
    $profile = XtcLoaderProfile::load($profile);
    $mappingName = XtcLoaderProfile::load($profile['mapping']);
    $mapping = XtcSearchLoaderSearch::load($mappingName['field_mapping']);
    return $mapping['mapping'];
//    return [
//      'mapping' => $mapping['mapping'],
//    ];
  }

  /**
   * @param $entity EntityInterface
   */
  protected static function getDefinitions($entity) {
    $entityType = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    return \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($entityType, $bundle);
  }


  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array
   */
  static protected function getIndexOptions(EntityInterface $entity) {
    return [];
  }

}
