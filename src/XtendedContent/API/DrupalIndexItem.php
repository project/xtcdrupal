<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\xtcsearch\XtendedContent\API\IndexItem;

class DrupalIndexItem extends DrupalIndexBase {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return string|void
   */
  public static function processIndex(EntityInterface $entity) {
    $message = '';
    $profile = static::getProfile('index', $entity->getEntityTypeId(), $entity->bundle());
    if (!empty($profile)) {
      if($entity instanceof ContentEntityBase){
        $content = $entity->getFields();
        try {
          $options = [
            'id' => $entity->id(),
            'index' => static::getIndexName(static::getIndexOptions($entity)),
            'mapping' => static::getMapping($profile),
            'definitions' => static::getDefinitions($entity),
          ];
          $handler = IndexItem::index($content, $profile, $options);

          $msg = $handler->content();
          if (is_string($msg)) {
            $message = $msg . ' ——— ' . 'Content indexed: '
              . $entity->getEntityTypeId() . ' — '
              . $entity->id() . ' in index: ' . $options['index'];
          }
          if (!empty($msg['result'])) {
            $message = $msg['result'] . ' ——— ' . 'Content indexed: '
              . $entity->getEntityTypeId() . ' — '
              . $entity->id() . ' in index: ' . $options['index'];
          }
          \Drupal::logger('xtcdrupal_search')->debug($message);
        } catch (\Exception $exception) {
          $message = $exception->getMessage() . ' (' . $exception->getCode() . ')';
          \Drupal::logger('xtcdrupal_search')->warning($message);
        } finally {
          return $message;
        }
      }
    }
    $message = "Profile not found — Content could not be indexed.";
    return $message;
  }

}
