<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityBase;
use Drupal\xtc\XtendedContent\API\ImporterBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DrupalImporterFile extends ImporterBase {

  /**
   * @param $name
   * @param array $options
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public static function process($name, $options = []) {
    $values = static::load($name, $options);
    $formatted = static::formatValues($values);
    $msg = static::save($formatted, $name, $options);
    return New JsonResponse($msg->getContent(), $msg->getStatusCode(), $msg->headers->all(), TRUE);
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return mixed|null
   */
  protected static function load($name, $options = []) {
    return XtcLoaderProfile::content($name, $options);
  }

  /**
   * @param $formatted
   * @param $name
   * @param array $options
   *
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws \Exception
   */
  protected static function save($formatted, $name, $options = []) {
    $ids = (!empty($options['ids'])) ? explode(',', $options['ids']) : NULL;
    $bundle = (!empty($options['bundle'])) ? $options['bundle'] : NULL;
    $msg = [];

    foreach ($formatted as $key => $content) {
      $importIds = (!empty($ids) && in_array($key, $ids)) ? TRUE : FALSE;
      if ($content instanceof EntityBase) {
        $importBundle = (!empty($bundle) && ($bundle == $content->bundle())) ? TRUE : FALSE;
        $importAll = (empty($bundle) && empty($ids)) ? TRUE : FALSE;
        if ($importAll || $importIds || $importBundle) {
          try {
            $status = $content->save();
            switch ($content->getEntityTypeId()){
              case 'media':
                $title = $content->getTypedData()->get('name')->getString();
                break;
              default:
                $title = $content->getTypedData()->get('title')->getString();
            }
            switch ($status) {
              case SAVED_NEW:
                $msg[] = 'Content ' . $content->getEntityTypeId()
                  . ' / ' . $content->bundle()
                  . ' / ' . $key
                  . ' has been created'
                  . ' : ' . $title
                  . '.';
                break;
              case SAVED_UPDATED:
                $msg[] = 'Content ' . $content->getEntityTypeId()
                  . ' / ' . $content->bundle()
                  . ' / ' . $key
                  . ' has been updated'
                  . ' : ' . $title
                  . '.';
                break;
              default:
            }
          } catch
          (\Exception $exception) {
            throw $exception;
          }
        }
      }
    }
    if (empty($msg)) {
      $msg = 'No content has been created / updated.';
    }
    $message = Json::encode($msg);
    return New Response($message);
  }

}
