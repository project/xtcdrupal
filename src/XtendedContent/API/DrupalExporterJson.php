<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Serialization\Yaml;

class DrupalExporterJson extends DrupalExporterFile {

  /**
   * @param $values
   *
   * @return mixed
   */
  protected static function formatValues($values) {
//    $contents = [];
//    foreach($values as $key => $value){
//      $contents[$key] = serialize($value);
//    }
//    return Json::encode($contents);
    return Json::encode($values);
  }

}
