<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


class DrupalImporterYaml extends DrupalImporterFile {

  /**
   * @param $values
   *
   * @return mixed|void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected static function formatValues($values) {
    $contents = [];
    foreach ($values as $key => $value) {
      $contents[$key] = unserialize($value);
    }
    return $contents;
  }

}
