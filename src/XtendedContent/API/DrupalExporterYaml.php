<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


use Drupal\Core\Serialization\Yaml;

class DrupalExporterYaml extends DrupalExporterFile {

  /**
   * @param $values
   *
   * @return mixed
   */
  protected static function formatValues($values) {
    $contents = [];
    foreach($values as $key => $value){
      $contents[$key] = serialize($value);
    }
    return Yaml::encode($contents);
  }

}
