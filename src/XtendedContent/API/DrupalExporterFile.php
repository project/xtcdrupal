<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


use Drupal\node\Entity\Node;
use Drupal\xtc\XtendedContent\API\ExporterBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcfile\XtendedContent\API\WriterFile;

class DrupalExporterFile extends ExporterBase {

  /**
   * @param $name
   * @param array $options
   *
   * @return mixed|null
   */
  protected static function load($name, $options = []) {
    $entity = $options['entity'] ?? 'node';
    $bundle = $options['bundle'] ?? '';
    $ids = $options['ids'] ?? '';
    $contents = [];
    if (!empty($ids)) {
      $entities = XtcLoaderProfile::content($entity, ['ids' => $ids]);
    } elseif (!empty($bundle)) {
      $entities = XtcLoaderProfile::content($entity, ['bundle' => $bundle]);
    } else {
      $entities = XtcLoaderProfile::content($entity);
    }

    foreach ($entities as $id => $entity) {
      $contents[$id] = $entity->toArray();
    }
    return $contents;
  }

  /**
   * @param $formatted
   * @param $name
   * @param array $options
   *
   * @return mixed|\Symfony\Component\HttpFoundation\Response
   */
  protected static function save($formatted, $name, $options = []){
    $profile = XtcLoaderProfile::load($name);

    $entity = $options['entity'] ?? 'node';
    $bundle = $options['bundle'] ?? '';
    $ids = $options['ids'] ?? '';
    $ext = ($profile['filetype']) ? '.' . $profile['filetype'] : '';
    if(empty($options['filename'])) {
      if (!empty($ids)) {
//        $options = array_merge($options, ['filename' => $entity . '-several' . $ext]);
        $filename = $entity . '-several' . $ext;
      }
      elseif (!empty($bundle)) {
//        $options = array_merge($options, ['filename' => $entity . '-' . $bundle . $ext]);
        $filename = $entity . '-' . $bundle . $ext;
      }
      else{
//        $options = array_merge($options, ['filename' => $entity . '-all' . $ext]);
        $filename = $entity . '-all' . $ext;
      }
      $options = array_merge($options, ['filename' => $filename]);
    }
    return WriterFile::write($formatted, $name, $options);
  }

}
