<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcdrupal\XtendedContent\API;


use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcsearch\Plugin\XtcHandler\SearchBase;
use Drupal\xtcsearch\XtendedContent\API\XtcSearchLoaderSearch;

class DrupalMapping extends DrupalIndexBase {

  /**
   * @param $name
   * @param array $options
   *
   * @return array
   */
  public static function buildIndex($name, $options = []) {
    $index = [];
    $search = XtcSearchLoaderSearch::load($name);
    $profiles = static::listProfiles($search);
    foreach ($profiles as $profileName => $values) {
      $profile = XtcLoaderProfile::load($profileName);
      $mappingProfile = XtcLoaderProfile::load($profile['mapping']);
      $fieldMapping = XtcSearchLoaderSearch::load($mappingProfile['field_mapping']);
      $options['fields'] = static::buildOptions($values);
      $options['mapping'] = $fieldMapping['mapping'];
//      $options['from'] = $fieldMapping['from'];
//      $options['to'] = $fieldMapping['to'];
      if (!empty($profile['mapping'])) {
        $handler = XtcLoaderHandler::getHandlerFromProfile($profile['mapping'], $options);
        if ($handler instanceof SearchBase) {
//          $index[$name] = $handler->process();
          $index[$profileName] = $handler->processContent();
        }
      }
    }
    return $index;
  }

  public static function buildOptions($values){
    $mapping = [];
    if (!empty($values['entities'])) {
      foreach ($values['entities'] as $entityType => $bundles) {
        $mapping[$entityType]['index'] = $values['index'];
        foreach ($bundles as $bundle) {
          $bundleFields = \Drupal::service('entity_field.manager')
            ->getFieldDefinitions($entityType, $bundle);
          foreach ($bundleFields as $fieldName => $currentField) {
            $mapping[$entityType]['fields'][$fieldName] = $currentField;
          }
        }
      }
    }
    return $mapping;
  }

  /**
   * @param $searchPlugin
   *
   * @return array
   */
  protected static function listProfiles($searchPlugin) {
    $index = [];
    foreach ($searchPlugin['index'] as $entityType => $bundles) {
      foreach ($bundles as $bundle => $name) {
        $index[$name]['entities'][$entityType][] = $bundle;
        $options = [
          'profile' => $name,
          'suffix' => $entityType,
        ];
        $index[$name]['index'] = static::getIndexName($options);
      }
    }
    return $index;
  }

}
