<?php

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


use Drupal\xtc\PluginManager\XtcFieldType\XtcFieldTypeInterface;
use Drupal\xtc\PluginManager\XtcFieldType\XtcFieldTypePluginBase;

/**
 * Plugin implementation of the xtc_fieldtype for FileType.
 *
 */
abstract class DrupalBase extends XtcFieldTypePluginBase
{

  /** @var \Drupal\Core\Field\FieldItemList */
  protected $field;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  public function setField() : XtcFieldTypeInterface {
    $this->field = $this->options['field'];
    return $this;
  }

  public function formatTo(){
  }

  /**
   * @return array|mixed|null
   */
  public function formatFrom(){
    /** @var \Drupal\Core\Field\FieldItemList */
    $field = $this->options['field'];
    $values = $field->getValue();
    if(count($values) > 1) {
      $items = [];
      foreach ($values as $value) {
        $items[] = $this->getValue($value);
      }
      return $items;
    }
    if(!empty($values[0])){
      return $this->getValue($values[0]);
    }
    return NULL;
  }

  /**
   * @param $value
   *
   * @return array|mixed|null
   */
  protected function getValue($value){
    if(!empty($value['value'])){
      return $value['value'];
    }
    return NULL;
  }

}
