<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use function GuzzleHttp\Psr7\str;

/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_boolean",
 *   label = @Translation("Boolean for XTC Drupal"),
 *   description = @Translation("Boolean for XTC Drupal.")
 * )
 */
class DrupalBoolean extends DrupalBase {

  /**
   * @param $value
   *
   * @return array|mixed|null
   */
  protected function getValue($value){
    return (bool) $value['value'];
  }

}
