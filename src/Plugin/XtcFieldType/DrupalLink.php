<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_link",
 *   label = @Translation("Link for XTC Drupal"),
 *   description = @Translation("Link for XTC Drupal.")
 * )
 */
class DrupalLink extends DrupalBase {

  /**
   * @param $value
   *
   * @return array|mixed|null
   */
  protected function getValue($value){
    return [
      'uri' => $value['uri'],
      'title' => $value['title'],
    ];
  }

}
