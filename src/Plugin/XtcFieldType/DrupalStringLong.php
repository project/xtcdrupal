<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_string_long",
 *   label = @Translation("String Long for XTC Drupal"),
 *   description = @Translation("String Long for XTC Drupal.")
 * )
 */
class DrupalStringLong extends DrupalString {

}
