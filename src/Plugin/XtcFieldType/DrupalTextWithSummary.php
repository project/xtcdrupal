<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_text_with_summary",
 *   label = @Translation("Text with Summary for XTC Drupal"),
 *   description = @Translation("Text with Summary for XTC Drupal.")
 * )
 */
class DrupalTextWithSummary extends DrupalString {

}
