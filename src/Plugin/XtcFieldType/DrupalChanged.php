<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_changed",
 *   label = @Translation("Changed Date for XTC Drupal"),
 *   description = @Translation("Changed Date for XTC Drupal.")
 * )
 */
class DrupalChanged extends DrupalDate {

}
