<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_text_long",
 *   label = @Translation("Text Long for XTC Drupal"),
 *   description = @Translation("Text Long for XTC Drupal.")
 * )
 */
class DrupalTextLong extends DrupalString {

}
