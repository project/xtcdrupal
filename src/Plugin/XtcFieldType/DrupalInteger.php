<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_integer",
 *   label = @Translation("Integer for XTC Drupal"),
 *   description = @Translation("Integer for XTC Drupal.")
 * )
 */
class DrupalInteger extends DrupalBase {

  /**
   * @param $value
   *
   * @return array|mixed|null
   */
  protected function getValue($value){
    return (int) $value['value'];
  }

}
