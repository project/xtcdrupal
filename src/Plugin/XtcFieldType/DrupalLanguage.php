<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_language",
 *   label = @Translation("Language for XTC Drupal"),
 *   description = @Translation("Language for XTC Drupal.")
 * )
 */
class DrupalLanguage extends DrupalBase {

}
