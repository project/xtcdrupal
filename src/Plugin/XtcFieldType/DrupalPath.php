<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_path",
 *   label = @Translation("Path for XTC Drupal"),
 *   description = @Translation("Path for XTC Drupal.")
 * )
 */
class DrupalPath extends DrupalBase {

  /**
   * @param $value
   *
   * @return array|mixed|null
   */
  protected function getValue($value){
    if(!empty($value['alias'])){
      return [
        'langcode' => $value['langcode'] ?? null,
        'alias' => $value['alias'],
        'pid' => (int) $value['pid'],
      ];
    }
    return NULL;
  }

}
