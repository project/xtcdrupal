<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\xtc\XtendedContent\API\XtcContentBuilder;

/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_entity_reference",
 *   label = @Translation("Entity Reference for XTC Drupal"),
 *   description = @Translation("Entity Reference for XTC Drupal.")
 * )
 */
class DrupalEntityReference extends DrupalBase {

  /**
   * @return array|null
   */
  public function formatFrom() {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $field */
    $field = $this->options['field'];
    if('dynamic' == $this->options['settings']['type']) {
      return $this->treatDynFrom($field);
    }
    else {
      return $this->treatFormatFrom($field);
    }

//    $fieldTypeName = $field->getFieldDefinition()->getType();
//    if(in_array($fieldTypeName, ['entity_reference','entity_reference_revisions'])) {
//      return $this->treatDynFrom($field);
//    }
//    else {
//      return $this->treatFormatFrom($field);
//    }
  }

  /**
   * @param \Drupal\Core\Field\EntityReferenceFieldItemList $field
   *
   * @return array|mixed|null
   */
  protected function treatDynFrom(EntityReferenceFieldItemList $field)  {
    /** @var []EntityBase $entities */
    $entities = $field->referencedEntities();

    if (count($entities) > 1) {
      $items = [];
      foreach ($entities as $key => $entity) {
        $items[] = $this->getDynamicValue($entity);
      }
      return $items;
    }
    if(!empty($entities[0])) {
      return $this->getDynamicValue($entities[0]);
    }
    return [];
  }

  /**
   * @param \Drupal\Core\Entity\EntityBase $entity
   *
   * @return array
   */
  protected function getDynamicValue(EntityBase $entity) : array {
    $item = [];
    $fieldNames = $this->options['field_names'];
    $bundles = $this->options['bundle_names'];
    $fields = $entity->toArray();

    foreach ($fields as $fieldName => $fieldValue) {
      if ('field_' == substr($fieldName, 0, 6)
        || ('taxonomy_term' == $entity->getEntityTypeId() && 'name' == $fieldName)
      ) {
        $settings = [
          'name' => $fieldName,
        ];
        $field = $entity->get($fieldName);
        $this->setBundle($item, $field, $bundles);
        //        $item['type'] = $bundles[$field->getEntity()->bundle()] ?? $field->getEntity()->bundle();
        $fieldTypeName = $field->getFieldDefinition()->getType();
        if(in_array($fieldTypeName, ['entity_reference','entity_reference_revisions'])) {
          foreach ($field->referencedEntities() as $fieldEntity) {
            $settings['type'] = 'dynamic';
            $settings['entity_type'] = $fieldEntity->getEntityTypeId();
          }
        }
        else {
          $settings['type'] = 'field';
        }
        $name = $fieldNames[$fieldName] ?? $fieldName;
        $newsettings = [
          'content' => [
            $name => $settings
          ],
          'field_names' => $fieldNames,
          'bundle_names' => $bundles,
        ];
        $newValue = XtcContentBuilder::buildContent($entity, $newsettings, TRUE);
        $item[$name] = $newValue[$name];
      }
    }

    if(!empty($this->options['settings']['bundles'][$entity->bundle()]['location'])) {
      foreach ($this->options['settings']['bundles'][$entity->bundle()]['location'] as $name => $subsettings) {
        if (!empty($subsettings['fullname_type']) && 'field' == $subsettings['fullname_type']) {
          $item[$name]['location']['fullname'] = $fields[$subsettings['fullname']][0]['value'];
        } else {
          if (!empty($subsettings['path_type']) && 'field' == $subsettings['path_type'] && !empty($subsettings['path'])) {
            $item[$name]['location']['path'] = $fields[$subsettings['path']][0]['value'] ?? '';
            $item[$name]['location']['name'] = $subsettings['name'] ?? '';
          }
          if (!empty($subsettings['name_type']) && 'field' == $subsettings['name_type'] && !empty($subsettings['name'])) {
            $item[$name]['location']['name'] = $fields[$subsettings['name']][0]['value'] ?? '';
          }
        }
      }
    }
    return $item;
  }

  /**
   * @param \Drupal\Core\Field\EntityReferenceFieldItemList $field
   *
   * @return array|mixed|null
   */
  protected function treatFormatFrom(EntityReferenceFieldItemList $field) {
    $entities = $field->referencedEntities();
    if (count($entities) > 1) {
      $items = [];
      foreach ($entities as $entity) {
        $items[] = $this->getValue($entity);
      }
      return $items;
    }
    if(!empty($entities[0])) {
      return $this->getValue($entities[0]);
    }
    return [];
  }

//  /**
//   * @param \Drupal\Core\Field\EntityReferenceFieldItemList $field
//   *
//   * @return array|mixed|null
//   */
//  protected function treatDynFrom(EntityReferenceFieldItemList $field) {
//    return $this->treatFormatFrom($field);
//  }

  /**
   * @param $value
   *
   * @return array|mixed|null
   */
  protected function getValue($value) {
    /** @var \Drupal\Core\Entity\EntityBase $entity */
    $entity = $value;
    $item = [];
    $options = $this->options;
    $settings = $options['settings'];
    if (
      $settings['entity_type'] == $entity->getEntityTypeId() &&
      !empty($settings['bundles'][$entity->bundle()])
    ) {
      $fields = $entity->toArray();
      if (!empty($entity->id())) {
        $item = [
          'target_id' => $entity->id(),
        ];
        if (!empty($fields['title'])) {
          $item['title'] = $fields['title'][0]['value'];
        }
        if(!empty($settings['bundles'][$entity->bundle()]['content'])) {
          $item = $this->buildValue($entity, $settings['bundles'][$entity->bundle()]);
        }

        if(!empty($settings['bundles'][$entity->bundle()]['location'])) {
          foreach ($settings['bundles'][$entity->bundle()]['location'] as $name => $subsettings) {
            if (!empty($subsettings['fullname_type']) && 'field' == $subsettings['fullname_type']) {
              $item[$name]['location']['fullname'] = $fields[$subsettings['fullname']][0]['value'];
            } else {
              if (!empty($subsettings['path_type']) && 'field' == $subsettings['path_type'] && !empty($subsettings['path'])) {
                $item[$name]['location']['path'] = $fields[$subsettings['path']][0]['value'] ?? '';
                $item[$name]['location']['name'] = $subsettings['name'] ?? '';
              }
              if (!empty($subsettings['name_type']) && 'field' == $subsettings['name_type'] && !empty($subsettings['name'])) {
                $item[$name]['location']['name'] = $fields[$subsettings['name']][0]['value'] ?? '';
              }
            }
          }
        }

        if($entity instanceof ConfigEntityBundleBase) {
          if (!empty($fields['name'])) {
            $item['name'] = $fields['name'];
          }
        }
      }
    }
    return $item;
  }

  /**
   * @param \Drupal\Core\Entity\EntityBase $entity
   * @param $settings
   *
   * @return mixed
   */
  protected function buildValue(EntityBase $entity, $settings) {
    return XtcContentBuilder::buildContent($entity, $settings, TRUE);
  }

  protected function setBundle(&$item, $field, $bundles) {
    if (!empty($bundles[$field->getEntity()->bundle()])) {
      if(is_string($bundles[$field->getEntity()->bundle()])) {
        $item['type'] = $bundles[$field->getEntity()->bundle()]
          ?? $field->getEntity()->bundle();
      }
      elseif(is_array($bundles[$field->getEntity()->bundle()])) {
        foreach ($bundles[$field->getEntity()->bundle()] as $key => $value) {
          $item[$key] = $value;
        }
      };
    }
  }

}
