<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_string",
 *   label = @Translation("String for XTC Drupal"),
 *   description = @Translation("String for XTC Drupal.")
 * )
 */
class DrupalString extends DrupalBase {

  /**
   * @param $value
   *
   * @return array|mixed|null
   */
  protected function getValue($value){
    if(!empty($value['value'])){
      if(!empty($value['format'])) {
        $val =  check_markup($value['value'], $value['format']);
        return $val->__toString();
      }
      return $value['value'];
    }
    return NULL;
  }

}
