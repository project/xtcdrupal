<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\xtc\XtendedContent\API\XtcContentBuilder;

/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_entity_reference_revisions",
 *   label = @Translation("Entity Reference Revisions for XTC Drupal"),
 *   description = @Translation("Entity Reference Revisions for XTC Drupal.")
 * )
 */
class DrupalEntityReferenceRevisions extends DrupalEntityReference {

  /**
   * @param \Drupal\Core\Entity\EntityBase $entity
   *
   * @return array
   */
  protected function getDynamicValue(EntityBase $entity) : array {
    $item = [];
    $fieldNames = $this->options['field_names'];
    $bundles = $this->options['bundle_names'];
    $fields = $entity->toArray();

    foreach ($fields as $fieldName => $fieldValue) {
      if ('field_' == substr($fieldName, 0, 6)) {
        $settings = [
          'name' => $fieldName,
        ];
        $field = $entity->get($fieldName);
        $this->setBundle($item, $field, $bundles);
//        $item['type'] = $bundles[$field->getEntity()->bundle()] ?? $field->getEntity()->bundle();
        $fieldTypeName = $field->getFieldDefinition()->getType();
        if(in_array($fieldTypeName, ['entity_reference','entity_reference_revisions'])) {
          foreach ($field->referencedEntities() as $fieldEntity) {
            $settings['type'] = 'dynamic';
            $settings['entity_type'] = $fieldEntity->getEntityTypeId();
          }
        }
        else {
          $settings['type'] = 'field';
        }
        $name = $fieldNames[$fieldName] ?? $fieldName;
        $newsettings = [
          'content' => [
            $name => $settings
          ],
          'field_names' => $fieldNames,
          'bundle_names' => $bundles,
        ];
        $newValue = XtcContentBuilder::buildContent($entity, $newsettings, TRUE);
        if (!empty($newValue[$name])) {
          $item[$name] = $newValue[$name];
        }
      }
    }

    if(!empty($this->options['settings']['bundles'][$entity->bundle()]['location'])) {
      foreach ($this->options['settings']['bundles'][$entity->bundle()]['location'] as $name => $subsettings) {
        if (!empty($subsettings['fullname_type']) && 'field' == $subsettings['fullname_type']) {
          $item[$name]['location']['fullname'] = $fields[$subsettings['fullname']][0]['value'];
        } else {
          if (!empty($subsettings['path_type']) && 'field' == $subsettings['path_type'] && !empty($subsettings['path'])) {
            $item[$name]['location']['path'] = $fields[$subsettings['path']][0]['value'] ?? '';
            $item[$name]['location']['name'] = $subsettings['name'] ?? '';
          }
          if (!empty($subsettings['name_type']) && 'field' == $subsettings['name_type'] && !empty($subsettings['name'])) {
            $item[$name]['location']['name'] = $fields[$subsettings['name']][0]['value'] ?? '';
          }
        }
      }
    }
    return $item;
  }

}
