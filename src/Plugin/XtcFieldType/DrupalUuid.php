<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "drupal_uuid",
 *   label = @Translation("UUID for XTC Drupal"),
 *   description = @Translation("UUID for XTC Drupal.")
 * )
 */
class DrupalUuid extends DrupalBase {

}
