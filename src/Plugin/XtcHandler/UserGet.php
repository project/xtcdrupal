<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "user_get",
 *   label = @Translation("User Get for XTC"),
 *   description = @Translation("User Get for XTC description.")
 * )
 */
class UserGet extends EntityGetBase {

  protected function getEntityType() {
    return 'user';
  }

}
