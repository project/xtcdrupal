<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


use Drupal\views\Views;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "view_get",
 *   label = @Translation("View Get for XTC"),
 *   description = @Translation("View Get for XTC description.")
 * )
 */
class ViewGet extends DrupalBase {

  /**
   * @var \Drupal\views\ViewExecutable
   */
  protected $view;

  /**
   * @var
   */
  protected $name;

  /**
   * @var array
   */
  protected $args;

  /**
   * @var string
   */
  protected $entityType;

  protected function initProcess() {
    $this->initArgs();
    $this->loadView();
  }

  protected function runProcess() {
    $this->view->setArguments($this->args);
    $this->view->setDisplay('default');
    $this->view->preExecute();
    $this->view->execute();
//    dump($this->view);
//    foreach ($this->view['#rows'] as $key => $row) {
//      $this->content[$key] = $this->view->
//    }
  }

  protected function adaptContent() {
//    dump($this);
  }

  protected function loadView() {
    $this->view = Views::getView($this->name);
  }

  protected function initArgs() {
    $this->name = $this->profile['name'];
    $this->args = $this->options['args'];
  }

}
