<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Entity\Webform;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "webformfields_get",
 *   label = @Translation("Webform Get for XTC"),
 *   description = @Translation("Webform Get for XTC description.")
 * )
 */
class WebformFieldsGet extends WebformGet {

  protected function adaptContent() {
    foreach ($this->content as $name => $webform) {
      if ($webform instanceof Webform) {
        $wf = $webform->toArray();
        $fields = Yaml::decode($wf['elements']);

        $this->content[$name] = $this->adaptFields($fields);
      }
    }
  }

  protected function adaptFields($fields) {
    return $fields;
  }

}
