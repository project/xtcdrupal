<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "node_get",
 *   label = @Translation("Node Get for XTC"),
 *   description = @Translation("Node Get for XTC description.")
 * )
 */
class NodeGet extends EntityGetBase {

  protected function getEntityType() {
    return 'node';
  }

}
