<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "term_get",
 *   label = @Translation("Term Get for XTC"),
 *   description = @Translation("Term Get for XTC description.")
 * )
 */
class TermGet extends EntityGetBase {

  protected function getEntityType() {
    return 'taxonomy_term';
  }

}
