<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


use Drupal\Core\Entity\ContentEntityBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderFieldType;

abstract class FieldBase extends EntityGetBase {

  /**
   * @var
   */
  var $fieldName;

  protected function initProcess(){
    $this->entityType = $this->getEntityType();
    $this->entity = \Drupal::entityTypeManager()->getStorage($this->entityType);
    $this->fieldName = $this->options['fieldname'];
  }

  protected function runProcess(){
    $entityContent = $this->entityLoad();
    $field = $entityContent->get($this->fieldName);
    $this->options['field'] = $field;
    $fieldType = XtcSearchLoaderFieldType::get($this->options['fieldtype'], $this->options);
    $this->content = $fieldType->formatFrom();
  }

  protected function adaptContent(){
  }


  /**
   * @return \Drupal\Core\Entity\ContentEntityBase
   */
  protected function entityLoad() : ContentEntityBase {
    $entityContent = NULL;
    if(!empty($this->options['content_id'])){
      $entityContent = $this->entity->load($this->options['content_id']);
    }
    return $entityContent;
  }

  protected function getEntityType() {
    return $this->options['entity_type_id'];
  }

  /**
   * @return string[]
   */
  public static function getStruct(): array {
    return ['entity_type_id', 'content_id', 'field'];
  }

}
