<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Entity\Webform;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "webform_get",
 *   label = @Translation("Webform Get for XTC"),
 *   description = @Translation("Webform Get for XTC description.")
 * )
 */
class WebformGet extends EntityGetBase {

  protected function getEntityType() {
    return 'webform';
  }

  protected function adaptContent() {
    foreach ($this->content as $name => $webform) {
      if ($webform instanceof Webform) {
        $this->content[$name] = $webform->toArray();
      }
    }
  }

}
