<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "paragraph_get",
 *   label = @Translation("Paragraph Get for XTC"),
 *   description = @Translation("Paragraph Get for XTC description.")
 * )
 */
class ParagraphGet extends EntityGetBase {

  protected function getEntityType() {
    return 'paragraph';
  }

}
