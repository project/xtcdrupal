<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "field_get",
 *   label = @Translation("Field Get for XTC"),
 *   description = @Translation("Field Get for XTC description.")
 * )
 */
class FieldGet extends FieldBase {


}
