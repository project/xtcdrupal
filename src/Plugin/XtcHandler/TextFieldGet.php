<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "textfield_get",
 *   label = @Translation("Text Field Get for XTC"),
 *   description = @Translation("Text Field Get for XTC description.")
 * )
 */
class TextFieldGet extends FieldGet {

  protected function getFieldType() {
    return 'text';
  }

  protected function getEntityType() {
    // TODO: Implement getEntityType() method.
  }

}
