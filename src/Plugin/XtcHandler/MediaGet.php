<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "media_get",
 *   label = @Translation("Media Get for XTC"),
 *   description = @Translation("Media Get for XTC description.")
 * )
 */
class MediaGet extends EntityGetBase {

  protected function getEntityType() {
    return 'media';
  }

}
