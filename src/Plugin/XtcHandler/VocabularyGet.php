<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "vocabulary_get",
 *   label = @Translation("Vocabulary Get for XTC"),
 *   description = @Translation("Vocabulary Get for XTC description.")
 * )
 */
class VocabularyGet extends EntityGetBase {

  protected function entityLoad(){
    return \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($this->options['vocabulary'], 0, NULL, true);
  }

  protected function getEntityType() {
    return 'vocabulary';
  }

}
