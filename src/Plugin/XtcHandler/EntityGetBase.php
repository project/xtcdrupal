<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


abstract class EntityGetBase extends DrupalBase {

  /**
   * @var string
   */
  protected $entityType;

  /**
   * @var \Drupal\Core\Entity\ContentEntityBase
   */
  protected $entity;

  protected function initProcess(){
    $this->entityType = $this->getEntityType();
    $this->entity = \Drupal::entityTypeManager()->getStorage($this->entityType);
  }

  protected function runProcess(){
    $this->content = $this->entityLoad();
  }

  protected function adaptContent(){
  }

  /**
   * @return \Drupal\Core\Entity\ContentEntityBase[]
   */
  protected function entityLoad(){
    if(!empty($this->options['ids'])){
      return $this->entity->loadMultiple(explode(',',$this->options['ids']));
    }

    $ids = \Drupal::entityQuery($this->entityType)->execute();
    return $this->entity->loadMultiple($ids);
  }

  abstract protected function getEntityType();

}
