<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcdrupal\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "block_get",
 *   label = @Translation("Block Get for XTC"),
 *   description = @Translation("Block Get for XTC description.")
 * )
 */
class BlockGet extends EntityGetBase {

  protected function getEntityType() {
    return 'block_content';
  }

}
