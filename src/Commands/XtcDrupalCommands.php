<?php


namespace Drupal\xtcdrupal\Commands;


use Drupal\Component\Serialization\Json;
use Drupal\xtcdrupal\XtendedContent\API\DrupalExporterYaml;
use Drupal\xtcdrupal\XtendedContent\API\DrupalImporterYaml;
use Drush\Commands\DrushCommands;

/**
 * Class XtcDrupalCommands
 *
 * @package Drupal\xtcdrupal\Commands
 */
class XtcDrupalCommands extends DrushCommands {

  /**
   * Import contents from any given content entity.
   *
   * @param string $entity
   *   Argument provided to the drush command.
   *
   * @command xtc:import-contents
   * @aliases xtcic
   * @options bundle Define a bundle.
   * @options ids Define a comma separated list of eid.
   * @usage xtc:import-contents node
   *   import all contents from node.
   * @usage xtc:import-contents node --bundle=article
   *   import the chosen content type.
   * @usage xtc:import-contents user --ids=1,2,3
   *   import the chosen users.
   */
  public function import($entity, $options = ['bundle' => NULL, 'ids' => NULL]) {
    $bundle = $options['bundle'] ?? '';
    $ids = $options['ids'] ?? '';
    $msg = DrupalImporterYaml::process('all-yaml2' . $entity, [
      'entity' => $entity,
      'bundle' => $bundle,
      'ids' => $ids,
    ]);
    $msg = Json::decode($msg->getContent());
    $this->output()->writeln($msg);
  }

  /**
   * Import contents from node entity.
   *
   * @command xtc:import-node
   * @aliases xtcin
   * @options bundle Define a bundle.
   * @options ids Define a comma separated list of eid.
   * @usage xtc:import-node
   *   import all contents from node.
   * @usage xtc:import-node --bundle=article
   *   import the chosen content type.
   * @usage xtc:import-node --ids=1,2,3
   *   import the chosen contents.
   */
  public function importNode($options = ['bundle' => NULL, 'ids' => NULL]) {
    $this->import('node', $options);
  }

  /**
   * Export contents from any given content entity.
   *
   * @param string $entity
   *   Argument provided to the drush command.
   *
   * @command xtc:export-contents
   * @aliases xtcxc
   * @options bundle Define a bundle.
   * @options ids Define a comma separated list of eid.
   * @usage xtc:export-contents node
   *   Export all contents from node.
   * @usage xtc:export-contents node --bundle=article
   *   Export the chosen content type.
   * @usage xtc:export-contents user --ids=1,2,3
   *   Export the chosen users.
   */
  public function export($entity, $options = ['bundle' => NULL, 'ids' => NULL]) {
//    $entity = $options['entity'] ?? 'node';
    $bundle = $options['bundle'] ?? '';
    $ids = $options['ids'] ?? '';
    $msg = DrupalExporterYaml::process($entity . '2yaml', [
      'entity' => $entity,
      'ids' => $ids,
    ]);
    $msg = Json::decode($msg->getContent());
    $this->output()->writeln($msg);
  }

  /**
   * Export contents from node entity.
   *
   * @command xtc:export-node
   * @aliases xtcxn
   * @options bundle Define a bundle.
   * @options ids Define a comma separated list of eid.
   * @usage xtc:export-node
   *   Export all contents from node.
   * @usage xtc:export-node --bundle=article
   *   Export the chosen content type.
   * @usage xtc:export-node --ids=1,2,3
   *   Export the chosen contents.
   */
  public function exportNode($options = ['bundle' => NULL, 'ids' => NULL]) {
    $this->export('node', $options);
  }

  /**
   * Export contents from block entity.
   *
   * @command xtc:export-block
   * @aliases xtcxb
   * @options bundle Define a bundle.
   * @options ids Define a comma separated list of eid.
   * @usage xtc:export-block
   *   Export all contents from block.
   * @usage xtc:export-block --bundle=article
   *   Export the chosen content type.
   * @usage xtc:export-block --ids=1,2,3
   *   Export the chosen contents.
   */
  public function exportBlock($options = ['bundle' => NULL, 'ids' => NULL]) {
    $this->export('block', $options);
  }

  /**
   * Export contents from media entity.
   *
   * @command xtc:export-media
   * @aliases xtcxm
   * @options bundle Define a bundle.
   * @options ids Define a comma separated list of eid.
   * @usage xtc:export-media
   *   Export all contents from media.
   * @usage xtc:export-media --bundle=article
   *   Export the chosen content type.
   * @usage xtc:export-media --ids=1,2,3
   *   Export the chosen contents.
   */
  public function exportMedia($options = ['bundle' => NULL, 'ids' => NULL]) {
    $this->export('media', $options);
  }

  /**
   * Export contents from user entity (only 'user' bundle is allowed in Drupal).
   *
   * @command xtc:export-user
   * @aliases xtcxu
   * @options ids Define a comma separated list of eid.
   * @usage xtc:export-user
   *   Export all users.
   * @usage xtc:export-user --ids=1,2,3
   *   Export the chosen users.
   */
  public function exportUser($options = ['ids' => NULL]) {
    $this->export('user', $options);
  }

}
